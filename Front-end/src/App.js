import {observer} from 'mobx-react';
import React from 'react';
import './App.css';
import {BrowserRouter as Router, Redirect, Route, Switch} from "react-router-dom";
import LoginForm from './Page/Login/Login';
import DashBoard from './Page/DashBoard/DashBoard';
import ProtectedRoute from './Router/ProtectedRoute';
import RegisterForm from './Page/Register/RegisterForm';

class App extends React.Component {

    render() {
        return (
            <Router>
                <Switch>
                    <Route path="/login">
                        <div className="container">
                            <LoginForm/>
                        </div>
                    </Route>
                    <Route path="/register">
                        <div className="container">
                            <RegisterForm/>
                        </div>
                    </Route>
                    <ProtectedRoute path="/dashboard">
                        <DashBoard/>
                    </ProtectedRoute>
                    <Route exact path="/">
                        <Redirect exact from="/" to="dashboard"/>
                    </Route>
                    <Route path="*">
                        <Redirect from="/" to="dashboard"/>
                    </Route>
                </Switch>
            </Router>
        );
    }
}

export default observer(App);
