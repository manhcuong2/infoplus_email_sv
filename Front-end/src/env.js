class Env {
    static API_URL = 'http://1.55.215.217:8989/';

    // User
    static URL_LOGIN = 'user/login';
    static URL_REGISTER = 'user/register';

    static sURL_SEND_EMAIL = 'email/compose';
}

export default Env;