import React, {Component} from "react";
import {NavbarData} from "./NavbarData";
import "./Navbar.css";
import {Link} from "react-router-dom";
import logo from "../assets/img/logo.jpg";

class Navbar extends Component {
    render() {
        return (
            <div className="Sidebar">
                <div className="div-logo">
                    <img className="img-logo" src={logo}/>
                </div>
                <nav>
                    <ul className="SidebarList">
                        {NavbarData.map((val, key) => {
                            return (
                                <li
                                    key={key}
                                    className="row"
                                    onClick={() => this.props.onClick(val)}
                                >
                                    <div id="title">
                                        <Link to={val.link} className="linkRouter">
                                            <div className="div-link">{val.title}</div>
                                        </Link>
                                    </div>
                                </li>
                            );
                        })}
                    </ul>
                </nav>
            </div>
        );
    }
}

export default Navbar;
