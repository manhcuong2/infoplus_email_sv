export const NavbarData = [
    {
        id: 1,
        title: "Send Email",
        link: "/send-email",
    },
    {
        id: 2,
        title: "Mailbox",
        link: "/mail-box",
    },
    {
        id: 3,
        title: "Sign out"
    }
];