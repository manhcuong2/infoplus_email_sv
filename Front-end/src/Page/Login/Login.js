import React from "react";
import SubmmitButton from "../../SubmmitButton";
import InputField from "../../InputField";
import "./Login.css";
import { Redirect } from "react-router-dom";
import logo from "../../assets/img/logo.jpg";
import LoadingMask from "react-loadingmask";
import "react-loadingmask/dist/react-loadingmask.css";
import { toast, ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Env from '../../env';
import ReactTimeout from 'react-timeout';

class LoginForm extends React.Component {
    constructor(prods) {
        super(prods);
        this.timer = null;
        this.state = {
            username: "",
            password: "",
            register: false,
            loading: false,
        };
    }

    setInputValue(property, val) {
        val = val.trim();

        this.setState({
            [property]: val,
        });
    }

    resetForm() {
        this.setState({
            username: "",
            password: "",
        });
    }

    logout = () => {
        localStorage.clear();
    }

    async doLogin() {
        if (this.state.username === "") {
            toast.error("Hãy nhập username");
            return;
        }
        if (this.state.password === "") {
            toast.error("Hãy nhập password");
            return;
        }

        this.setState({
            loading: true,
        });

        const requestOptions = {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                Accept: "application/json",
            },
            body: JSON.stringify({
                userName: this.state.username,
                password: this.state.password,
            }),
        };

        fetch(Env.API_URL + Env.URL_LOGIN, requestOptions)
            .then((res) => {
                if (res.status === 200) {
                    localStorage.clear();
                    localStorage.setItem("token", true);
                    res.text().then((token) => {
                        localStorage.setItem("token", token);
                    });
                    localStorage.setItem("username", this.state.username);
                    localStorage.setItem("password", this.state.password);
                    this.setState({
                        loading: false,
                    });

                    clearTimeout(this.timer);
                    this.timer = this.props.setTimeout(this.logout, 60*60*1000);
                } else {
                    this.setState({
                        loading: false,
                    });
                    this.resetForm();
                    toast.error("Username hoặc password không đúng");
                }
            })
            .catch((err) => {
                console.log(err);
            });
    }

    doRegister = () => {
        this.setState({
            register: true,
        });
    };

    handleKeyDown = (e) => {
       if (e.key == "Enter") {
            this.doLogin();
       }
    }

    render() {
        if (localStorage.getItem("token")) {
            return <Redirect to="/" />;
        }
        if (this.state.register === true) {
            return <Redirect to="/register" />;
        }

        if (this.state.loading === true) {
            return (
                <LoadingMask loading={true} text={"loading..."}>
                    <div style={{ width: 500, height: 300 }}>Processing</div>
                </LoadingMask>
            );
        } else {
            return (
                <div className="loginForm">
                    <div className="div-logo-login">
                        <img className="img-logo-login" src={logo} />
                    </div>
                    <div className="div-lbl-login">
                        <label>Log in</label>
                    </div>
                    <InputField
                        type="text"
                        placeholder="Username"
                        value={this.state.username ? this.state.username : ""}
                        onChange={(val) => this.setInputValue("username", val)}
                        onKeyDown={(e) => this.handleKeyDown(e)}
                    />
                    <InputField
                        type="password"
                        placeholder="Password"
                        value={this.state.password ? this.state.password : ""}
                        onChange={(val) => this.setInputValue("password", val)}
                        onKeyDown={(e) => this.handleKeyDown(e)}
                    />
                    <SubmmitButton
                        text="Login"
                        disabled={this.state.buttonDisabled}
                        onClick={() => this.doLogin()}
                    />
                    <div className="div-login">
                        <label className="lbl-login" onClick={this.doRegister}>
                            Register
                        </label>
                    </div>
                    <ToastContainer position={toast.POSITION.TOP_RIGHT} />
                </div>
            );
        }
    }
}

export default ReactTimeout(LoginForm);
