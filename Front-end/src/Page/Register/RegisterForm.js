import React from "react";
import SubmmitButton from "../../SubmmitButton";
import InputField from "../../InputField";
import "./Register.css";
import { Redirect } from "react-router-dom";
import { toast, ToastContainer } from "react-toastify";
import Env from '../../env';

class RegisterForm extends React.Component {
    constructor(prods) {
        super(prods);
        this.state = {
            username: "",
            password: "",
            repassword: "",
            buttonDisabled: false,
            isRegister: false,
        };
    }

    setInputValue(property, val) {
        val = val.trim();

        this.setState({
            [property]: val,
        });
    }

    resetForm() {
        this.setState({
            username: "",
            password: "",
            repassword: "",
            buttonDisabled: false,
            isRegister: false,
        });
    }

    async doRegister() {
        if (this.state.username === "") {
            toast.error("Hãy nhập username");
            return;
        }
        if (this.state.password === "") {
            toast.error("Hãy nhập password");
            return;
        }
        if (this.state.repassword === "") {
            toast.error("Hãy nhập lại password");
            return;
        }

        if (this.state.password !== this.state.repassword) {
            toast.error("Password không khớp nhau");
            return;
        }

        const requestOptions = {
            method: "PUT",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify({
                userName: this.state.username,
                password: this.state.password,
                rePassword: this.state.repassword,
            }),
        };
        fetch(Env.API_URL + Env.URL_REGISTER, requestOptions)
            .then((res) => {
                switch (res.status) {
                    case 304:
                        toast.error("Email không tồn tại");
                        break;
                    case 409:
                        toast.error("Email đã được đăng ký");
                        break;
                    case 201:
                        toast.success("Đăng ký thành công");
                        this.setState({
                            isRegister: true,
                        });
                        break;
                    default:
                        toast.error("Có lỗi xảy ra trong quá trình xử lý");
                        break;
                }
            })
            .catch((err) => {
                console.log(err);
            });
    }

    onBackToLogin = () => {
        this.setState({
            isRegister: true,
        });
    };

    handleKeyDown = (e) => {
        if (e.key == "Enter") {
             this.doRegister();
        }
     }

    render() {
        if (this.state.isRegister === true) {
            return <Redirect to="/login" />;
        }
        return (
            <div className="registerForm">
                Register
                <InputField
                    type="text"
                    placeholder="Username"
                    value={this.state.username ? this.state.username : ""}
                    onChange={(val) => this.setInputValue("username", val)}
                    onKeyDown={(e) => this.handleKeyDown(e)}
                />
                <InputField
                    type="password"
                    placeholder="Password"
                    value={this.state.password ? this.state.password : ""}
                    onChange={(val) => this.setInputValue("password", val)}
                    onKeyDown={(e) => this.handleKeyDown(e)}
                />
                <InputField
                    type="password"
                    placeholder="Re-enter password"
                    value={this.state.repassword ? this.state.repassword : ""}
                    onChange={(val) => this.setInputValue("repassword", val)}
                    onKeyDown={(e) => this.handleKeyDown(e)}
                />
                <SubmmitButton
                    text="Register"
                    disabled={this.state.buttonDisabled}
                    onClick={() => this.doRegister()}
                />
                <div className="div-login">
                    <label className="lbl-login" onClick={this.onBackToLogin}>
                        Login
                    </label>
                </div>
                <ToastContainer position={toast.POSITION.TOP_RIGHT} />
            </div>
        );
    }
}

export default RegisterForm;
