import React, { Component } from "react";
import "./DashBoard.css";
import "@trendmicro/react-sidenav/dist/react-sidenav.css";
import { BrowserRouter as Router, Redirect, Route, Switch } from "react-router-dom";
import Navbar from "../../Component/Navbar";
import MailBox from "../MailBox/MailBox";
import EmailService from "../EmailService/EmailService";

class DashBoard extends Component {
    constructor(props) {
        super(props);
        this.timer = null;
        this.state = {
            clicked: false,
            islogout: false,
        };
    }

    handleClick = () => {
        this.setState({ clicked: !this.state.clicked });
    };

    onClickItemMenu = (val) => {
        if (val.id === 3 || !localStorage.getItem("token")) {
            localStorage.clear();
            this.setState({
                islogout: true,
            });
        }
    };

    render() {
        if (this.state.islogout) {
            return <Redirect to="/email-service" />;
        }
        return (
            <div className="div-content">
                <Router>
                    <div className="div-vertical-1">
                        <Navbar onClick={(val) => this.onClickItemMenu(val)} />
                    </div>
                    <div className="div-vertical-2">
                        <div className="div-lbl-welcome">
                            <label className="lbl-welcome">
                                Welcome {localStorage.getItem("username")}
                            </label>
                        </div>
                        <Switch>
                            <Route
                                exact
                                path="/dashboard"
                                render={() => {
                                    return <Redirect to="/send-email" />;
                                }}
                            />
                            <Route exact path="/send-email">
                                <EmailService />
                            </Route>
                            <Route exact path="/mail-box">
                                <MailBox />
                            </Route>
                        </Switch>
                    </div>
                </Router>
            </div>
        );
    }
}

export default DashBoard;
