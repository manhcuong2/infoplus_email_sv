import React from "react";
import "./EmailService.css";
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";
import "react-datepicker/dist/react-datepicker.css";
import DatePicker from "react-datepicker";
import { ExcelRenderer } from "react-excel-renderer";
import validator from "validator";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import CKEditor from "ckeditor4-react";
import ReactModal from "react-modal";
import IconClose from '@material-ui/icons/Close';
import Env from '../../env';

class EmailService extends React.Component {
    constructor() {
        super();
        this.state = {
            valueSubject: "",
            valueContent: "",
            toEmail: "",
            fromEmail: localStorage.getItem("username"),
            fromName: localStorage.getItem("username"),
            dateSend: new Date(),
            showModalFileExcel: false,
            listEmailExcelStr: ""
        };
    }

    componentDidMount() { }

    resetForm = () => {
        this.setState({
            valueSubject: "",
            valueContent: "",
            toEmail: "",
            fromEmail: localStorage.getItem("username"),
            fromName: localStorage.getItem("username"),
            dateSend: new Date(),
        });
    };

    sendEmail = () => {
        if (validator.isEmpty(this.state.valueSubject)) {
            window.location.reload();
            return;
        }
        if (validator.isEmpty(this.state.valueContent)) {
            toast.error("Hãy nhập nội dung email");
            return;
        }

        if (validator.isEmpty(this.state.toEmail)) {
            toast.error("Hãy nhập danh sách email cần gửi");
            return;
        }

        this.validateEmail().then((res) => {
            if (res <= 0) {
                let arrEmail = this.state.toEmail.split(",").map(function (item) {
                    return item.trim();
                });
                let listEmail = {
                    fromEmail: this.state.fromEmail,
                    toEmailList: arrEmail,
                    title: this.state.valueSubject,
                    content: this.state.valueContent,
                    senderNm: this.state.fromName,
                    requestDt: this.state.dateSend,
                };

                const requestOptions = {
                    method: "POST",
                    headers: new Headers({
                        "Content-Type": "application/json",
                        Authorization: "Bearer " + localStorage.getItem("token"),
                    }),
                    body: JSON.stringify(listEmail),
                };

                fetch(Env.API_URL + Env.sURL_SEND_EMAIL, requestOptions)
                    .then((res) => {
                        toast.success("Gửi email thành công!");
                        this.resetForm();
                    })
                    .catch((err) => {
                        toast.error(err);
                    });
            } else {
                toast.error("Danh sách email tồn tại email không đúng định dạng");
            }
        });
    };

    validateEmail = () => {
        return new Promise((resolve, reject) => {
            let arrToEmail = this.state.toEmail.split(",");
            let countEmail = 0;
            if (arrToEmail.length > 0) {
                arrToEmail.forEach((email, index) => {
                    let check = new RegExp(
                        /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,15}/g
                    ).test(email);
                    if (!check) {
                        countEmail++;
                    }
                    if (index === arrToEmail.length - 1) resolve(countEmail);
                });
            }
        });
    };

    onChangeDateSend = (date) => {
        this.setState({
            dateSend: date,
        });
    };

    onChangeSubject = (e) => {
        this.setState({
            valueSubject: e.target.value,
        });
    };

    onChangeContentEmail(e) {
        this.setState({
            valueContent: e.editor.getData(),
        });
    }

    onChangeToEmail = (e) => {
        this.setState({
            toEmail: e.target.value,
        });
    };

    handleKeyDownToEmail = (evt) => {
        if (evt.key === ",") {
            evt.preventDefault();
        }
        if (evt.key === " ") {
            this.setState({
                toEmail: this.state.toEmail + ",",
            });
        }
    };

    onChangeFromName = (e) => {
        this.setState({
            fromName: e.target.value,
        });
    };

    onChangeFromEmail = (e) => {
        this.setState({
            fromEmail: e.target.value,
        });
    };

    chooseFile = () => {
        this.setState({ showModalFileExcel: true });
    };

    closeModal = () => {
        this.setState({ showModalFileExcel: false });
    }

    addListEmailFromExcel = () => {
        this.setState({
            toEmail: this.state.listEmailExcelStr
        });
        this.setState({ listEmailExcelStr: "" });
        this.setState({ showModalFileExcel: false });
    };

    fileHandler = (event) => {
        let fileObj = event.target.files[0];
        ExcelRenderer(fileObj, (err, resp) => {
            if (err) {
                console.log(err);
            } else {
                this.setState({
                    cols: resp.cols,
                    rows: resp.rows,
                });
                let emailStr = "";
                const emailRows = this.state.rows;
                emailRows.map((email) => {
                    if (email.length == 0) {
                        return;
                    }
                    let emEmail = email[0].trim();
                    if (!validator.isEmpty(emEmail) && validator.isEmail(emEmail)) {
                        emailStr = emEmail + "," + emailStr;
                    }
                });
                if (emailStr.length > 1) {
                    emailStr = emailStr.substr(0, emailStr.length - 1);
                }

                this.setState({
                    listEmailExcelStr: emailStr
                });
            }
        });
    };

    render() {
        return (
            <div className="div-container">
                <div className="div-email">
                    <div className="subject">
                        <div className="div-lbl-subject">
                            <label>Subject</label>
                        </div>
                        <div className="div-input-subject">
                            <input
                                className="inputSubject"
                                value={this.state.valueSubject}
                                onChange={this.onChangeSubject}
                            />
                        </div>
                    </div>
                    <div className="formEmail">
                        <div className="div-lbl-subject">
                            <label>Content</label>
                        </div>
                        <div className="div-formEmail">
                            <CKEditor
                                data={this.state.valueContent}
                                value={this.state.valueContent}
                                onChange={(e) => this.onChangeContentEmail(e)}
                            />
                        </div>
                    </div>
                </div>
                <div className="form-user">
                    <div className="div-label-date-send">
                        <label>Date send</label>
                    </div>
                    <div className="div-date-piker">
                        <DatePicker
                            minDate={new Date()}
                            selected={this.state.dateSend}
                            onChange={(date) => this.onChangeDateSend(date)}
                            showTimeSelect
                            dateFormat="dd/MM/yyyy HH:mm:ss"
                        />
                    </div>
                    <div className="div-label-to-email">
                        <label>To email</label>
                    </div>
                    <div className="div-input-to-email">
                        <div className="div-inside-to-email-1">
                            <input
                                className="inputSubject"
                                value={this.state.toEmail}
                                onChange={this.onChangeToEmail}
                                onKeyDown={this.handleKeyDownToEmail}
                            />
                        </div>
                        <div className="div-inside-to-email-2">
                            <button
                                className="btn-choose-from-file"
                                onClick={() => this.chooseFile()}
                            >
                                Choose file
              </button>
                        </div>
                    </div>
                    <div className="div-from-name">
                        <label>From name</label>
                    </div>
                    <div className="div-input-from-name">
                        <input
                            className="inputSubject"
                            value={this.state.fromName}
                            onChange={this.onChangeFromName}
                        />
                    </div>
                    <div className="div-from-email">
                        <label>From email</label>
                    </div>
                    <div className="div-input-from-email">
                        <input
                            disabled={true}
                            className="inputSubject"
                            value={this.state.fromEmail}
                            onChange={this.onChangeFromEmail}
                        />
                    </div>
                    <div className="div-btn-send-email">
                        <button className="btn-send-email" onClick={() => this.sendEmail()}>
                            Send email
            </button>
                    </div>
                </div>
                <ToastContainer position={toast.POSITION.TOP_RIGHT} />
                <ReactModal
                    className="modal-choose-file"
                    isOpen={this.state.showModalFileExcel}
                    contentLabel="Minimal Modal Example"
                    ariaHideApp={false}
                >
                    <div className="modal-choose-file-content">
                        <div className="div-header">
                            <label>Add email from excel file</label>
                            <IconClose className="ic-close"
                                onClick={this.closeModal}
                            ></IconClose>
                        </div>
                        <div className="div-choose-file">
                            <div className="div-choose-file-in-1">
                                <label className="lbl-file">File</label>
                            </div>
                            <div className="div-choose-file-in-2">
                                <input
                                    className="input-file"
                                    type="file"
                                    id="importExcel"
                                    onChange={this.fileHandler.bind(this)}
                                />
                            </div>
                        </div>
                        <div className="div-footer">
                            <button
                                className="btn-close-modal-choose-file"
                                onClick={this.addListEmailFromExcel}
                            >
                                ADD
              </button>
                        </div>
                    </div>
                </ReactModal>
            </div>
        );
    }
}

export default EmailService;
