import React from 'react';

class SubmmitButton extends React.Component {
    render() {
        return (
            <div className="submmitButton">
                <button
                    className='btn'
                    disabled={this.props.disabled}
                    onClick={() => this.props.onClick()}
                >
                    {this.props.text}
                </button>
            </div>
        );
    }

}

export default SubmmitButton;
