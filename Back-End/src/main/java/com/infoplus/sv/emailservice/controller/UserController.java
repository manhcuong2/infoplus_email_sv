package com.infoplus.sv.emailservice.controller;

import com.infoplus.sv.emailservice.config.Config;
import com.infoplus.sv.emailservice.model.UserModel;
import com.infoplus.sv.emailservice.security.JwtTokenUtil;
import com.infoplus.sv.emailservice.services.EmailService;
import com.infoplus.sv.emailservice.services.UserService;
import com.infoplus.sv.emailservice.util.Validator;
import lombok.extern.log4j.Log4j;
import net.minidev.json.JSONObject;
import org.apache.commons.validator.routines.EmailValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@Log4j
@CrossOrigin(origins = "*", allowedHeaders = "*")
@Controller
@RequestMapping("/user")
@Validated
public class UserController {
    public static final Logger logger = LoggerFactory.getLogger(UserController.class);
    @Autowired
    private EmailService emailService;

    @Autowired
    private UserService userService;

    @Autowired
    private Config fbkConfig;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @CrossOrigin
    @GetMapping(value = "/test")
    public ResponseEntity<?> testAPI() {
        return new ResponseEntity<>("OK", HttpStatus.OK);
    }

    @CrossOrigin
    @PutMapping(value = "/register")
    public ResponseEntity<?> registerUser(@RequestBody UserModel userModel) {
        String userName = userModel.getUserName();
        String password = userModel.getPassword();
        String repassword = userModel.getRePassword();
        if (!Validator.validateStrings(userName, password, repassword)) {
            return new ResponseEntity<>("UserName, Password, RePassword can not null", HttpStatus.NOT_ACCEPTABLE);
        }
        if (!EmailValidator.getInstance().isValid(userName)) {
            return new ResponseEntity<>("UserName is not accept", HttpStatus.NOT_MODIFIED);
        }
        if (!password.equals(repassword)) {
            return new ResponseEntity<>("Password and RePassword not same", HttpStatus.NOT_ACCEPTABLE);
        }
        if (userService.isUser(userName)) {
            return new ResponseEntity<>("User is existing", HttpStatus.CONFLICT);
        }
        int userId = userService.createUser(userName, password);
        if (userId == 0) {
            return new ResponseEntity<>("Can not create User", HttpStatus.NOT_ACCEPTABLE);
        }
        final String token = jwtTokenUtil.generateToken(userName);
        return new ResponseEntity<>(token, HttpStatus.CREATED);
    }

    @CrossOrigin
    @PostMapping(value = "/login", consumes = "application/json", produces = "application/json")
    public ResponseEntity<?> login(@RequestBody UserModel userModel) {
        String userName = userModel.getUserName();
        String password = userModel.getPassword();
        if (!Validator.validateStrings(userName, password)) {
            return new ResponseEntity<>("UserName, Password can not null", HttpStatus.NOT_ACCEPTABLE);
        }
        if (!EmailValidator.getInstance().isValid(userName)) {
            return new ResponseEntity<>("UserName is not accept", HttpStatus.NOT_ACCEPTABLE);
        }

        boolean isUser = userService.isUser(userName);
        if (!isUser) {
            return new ResponseEntity<>("User is not Existing", HttpStatus.NOT_ACCEPTABLE);
        }
        boolean isPwd = userService.isPwd(userName, password);
        if (!isPwd) {
            return new ResponseEntity<>("Password not correct", HttpStatus.NOT_ACCEPTABLE);
        }
        final String token = jwtTokenUtil.generateToken(userName);
        return new ResponseEntity<>(token, HttpStatus.OK);
    }
}
