package com.infoplus.sv.emailservice.controller;

import com.infoplus.sv.emailservice.config.Config;
import com.infoplus.sv.emailservice.model.EmailModel;
import com.infoplus.sv.emailservice.services.EmailService;
import com.infoplus.sv.emailservice.util.Validator;
import lombok.extern.log4j.Log4j;
import org.apache.commons.validator.routines.EmailValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Email;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

@Log4j
@CrossOrigin(origins = "*", allowedHeaders = "*")
@Controller
@RequestMapping("/email")
@Validated
public class EmailController {
    public static final Logger logger = LoggerFactory.getLogger(EmailController.class);
    @Autowired
    private EmailService emailService;

    @Autowired
    private Config fbkConfig;


    @CrossOrigin
    @PostMapping(value = "/compose")
    public ResponseEntity<?> compose(@RequestBody EmailModel emailModel) {
        String fromEmail = emailModel.getFromEmail();
        List<String> toEmailList = emailModel.getToEmailList();
        String title = emailModel.getTitle();
        String content = emailModel.getContent();
        String senderNm = emailModel.getSenderNm();
        Date requestDt = emailModel.getRequestDt();

        if (!Validator.validateStrings(fromEmail, title, content)) {
            return new ResponseEntity<>("From Email,To Email, Title, Content can not null", HttpStatus.NOT_ACCEPTABLE);
        }
        if (!Validator.validateCollection(toEmailList)){
            return new ResponseEntity<>("To Email can not empty", HttpStatus.NOT_ACCEPTABLE);
        }
        if (!EmailValidator.getInstance().isValid(fromEmail)) {
            return new ResponseEntity<>("From Email is invalid", HttpStatus.NOT_ACCEPTABLE);
        }
        if (requestDt == null) {
            requestDt = new Date();
        }
        for (String toEmail : toEmailList) {
            if (!EmailValidator.getInstance().isValid(toEmail)) {
                return new ResponseEntity<>("To Email is invalid", HttpStatus.NOT_ACCEPTABLE);
            }
            emailService.createEmail(fromEmail, toEmail, title, content, senderNm, requestDt);
        }

        return new ResponseEntity<>("Created", HttpStatus.CREATED);
    }

    @Scheduled(cron = "0 0/2 * * * ?")
//    @GetMapping(value = "/send")
    public ResponseEntity<?> scheduleSendEmail() throws ParseException {
        List<EmailModel> emailModels = emailService.getEmails();

        for (EmailModel emailModel : emailModels) {
            int pushId = emailModel.getPushId();
            String fromEmail = emailModel.getFromEmail();
            String toEmail = emailModel.getToEmail();
            String title = emailModel.getTitle();
            String content = emailModel.getContent();
            Date rqDate = emailModel.getRequestDt();
            Date todayDate = new Date();
            if (rqDate.before(todayDate)){
                emailService.sendHTML(fromEmail, toEmail, title, content);
                emailService.saveHis(pushId);
                emailService.delete(pushId);
            }
        }
        return new ResponseEntity<>("Sent", HttpStatus.OK);
    }

}
