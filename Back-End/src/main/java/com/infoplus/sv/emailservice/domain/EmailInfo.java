package com.infoplus.sv.emailservice.domain;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "email")
@Data
public class EmailInfo implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int pushId;
    @Column(nullable = true)
    private int emailTp;
    @Column(nullable = false)
    private String fromEmail;
    @Column(nullable = false)
    private String toEmail;
    @Column(nullable = false)
    private String title;
    @Column(nullable = false)
    @Lob
    private String content;
    private String senderNm;
    private long reqDt;
    private int status = 0;
}
