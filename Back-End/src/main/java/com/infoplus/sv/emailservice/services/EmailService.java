package com.infoplus.sv.emailservice.services;


import com.infoplus.sv.emailservice.model.EmailModel;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

public interface EmailService {

    int createEmail(String fromEmail, String toEmail, String title, String content, String senderNm, Date requestDt);

    List<EmailModel> getEmails() throws ParseException;

    void sendText(String from, String to, String subject, String body);

    void sendHTML(String from, String to, String subject, String body);

    void saveHis(int pushId);

    void delete(int pushId);
}
