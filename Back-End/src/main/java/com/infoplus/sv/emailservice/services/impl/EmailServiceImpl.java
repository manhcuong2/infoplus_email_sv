package com.infoplus.sv.emailservice.services.impl;

import com.infoplus.sv.emailservice.config.Config;
import com.infoplus.sv.emailservice.domain.EmailInfo;
import com.infoplus.sv.emailservice.model.EmailModel;
import com.infoplus.sv.emailservice.repositories.EmailRepo;
import com.infoplus.sv.emailservice.services.EmailService;
import com.infoplus.sv.emailservice.util.DateUtils;
import com.sendgrid.Method;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;
import com.sendgrid.helpers.mail.Mail;
import com.sendgrid.helpers.mail.objects.Content;
import com.sendgrid.helpers.mail.objects.Email;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class EmailServiceImpl implements EmailService {
    @Autowired
    private EmailRepo emailRepo;
    @Autowired
    private Config config;


    @Override
    public int createEmail(String fromEmail, String toEmail, String title, String content, String senderNm, Date requestDt) {
        EmailInfo emailInfo = new EmailInfo();
        emailInfo.setFromEmail(fromEmail);
        emailInfo.setToEmail(toEmail);
        emailInfo.setTitle(title);
        emailInfo.setContent(content);
        emailInfo.setSenderNm(senderNm);
        emailInfo.setReqDt(requestDt.getTime());
        int pushId = emailRepo.createEmail(emailInfo);
        return pushId;
    }

    @Override
    public List<EmailModel> getEmails() {
        List<EmailModel> emailModels = new ArrayList<>();
        List<EmailInfo> emailInfos = emailRepo.getEmails();
        for (EmailInfo emailInfo : emailInfos) {
            EmailModel emailModel = new EmailModel();
            emailModel.setPushId(emailInfo.getPushId());
            emailModel.setFromEmail(emailInfo.getFromEmail());
            emailModel.setToEmail(emailInfo.getToEmail());
            emailModel.setTitle(emailInfo.getTitle());
            emailModel.setContent(emailInfo.getContent());
            emailModel.setSenderNm(emailInfo.getSenderNm());
            String datemm = DateUtils.formatDateTimeWithTimezone(emailInfo.getReqDt(), "GMT+7");
            emailModel.setRequestDt(DateUtils.stringToDate(datemm));
            emailModels.add(emailModel);
        }

        return emailModels;
    }

    @Override
    public void sendText(String from, String to, String subject, String body) {
        Response response = sendEmail(from, to, subject, new Content("text/plain", body));
        System.out.println("Status Code: " + response.getStatusCode() + ", Body: " + response.getBody() + ", Headers: " + response.getHeaders());
    }

    @Override
    public void sendHTML(String from, String to, String subject, String body) {
        Response response = sendEmail(from, to, subject, new Content("text/html", body));
        System.out.println("Status Code: " + response.getStatusCode() + ", Body: " + response.getBody() + ", Headers: " + response.getHeaders());
    }

    private Response sendEmail(String from, String to, String subject, Content content) {
        String SENDGRID_API_KEY = config.getSendGridAPIKey();
        SendGrid sendGrid = new SendGrid(SENDGRID_API_KEY);
        Mail mail = new Mail(new Email(from), subject, new Email(to), content);
        mail.setReplyTo(new Email(from));
        Request request = new Request();
        Response response = null;
        try {
            request.setMethod(Method.POST);
            request.setEndpoint("mail/send");
            request.setBody(mail.build());
            response = sendGrid.api(request);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return response;
    }

    @Override
    public void saveHis(int pushId) {
        emailRepo.saveHis(pushId);
    }

    @Override
    public void delete(int pushId) {
        emailRepo.delEmail(pushId);
    }
}
