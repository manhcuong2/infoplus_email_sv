package com.infoplus.sv.emailservice.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
@Data
public class Config {

    @Value("${sendgrid.api.key}")
    String sendGridAPIKey;


}
