package com.infoplus.sv.emailservice.repositories;


import com.infoplus.sv.emailservice.domain.EmailInfo;

import java.util.List;

public interface EmailRepo {
    EmailInfo getFbkFileInfo(String directoryPath);

    List<EmailInfo> getEmails();

    EmailInfo getEmail(long id);

    void updateEmail(EmailInfo fbkFile);

    void delEmail(int id);

    boolean isFileExist(EmailInfo emailInfo);

    int createEmail(EmailInfo emailModel);

    void saveHis(int pushId);
}
