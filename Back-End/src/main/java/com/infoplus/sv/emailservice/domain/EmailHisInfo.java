package com.infoplus.sv.emailservice.domain;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "emailHis")
@Data
public class EmailHisInfo implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Id
    private int pushId;
    @Column(nullable = true)
    private int emailTp;
    @Column(nullable = false)
    private String fromEmail;
    @Column(nullable = false)
    private String toEmail;
    @Column(nullable = false)
    private String title;
    @Column(nullable = false)
    @Lob
    private String content;
    private String senderNm;
    private long reqDt;
    private long sentDt;
    private int status;
}