package com.infoplus.sv.emailservice.repositories.impl;

import com.infoplus.sv.emailservice.domain.EmailHisInfo;
import com.infoplus.sv.emailservice.domain.EmailInfo;
import com.infoplus.sv.emailservice.repositories.EmailRepo;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Date;
import java.util.List;


@Repository
@Transactional
public class EmailRepoImpl implements EmailRepo {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public EmailInfo getFbkFileInfo(String directoryPath) {
        return null;
    }

    @Override
    public List<EmailInfo> getEmails() {
        List<EmailInfo> emailInfos = entityManager.createQuery("FROM EmailInfo").getResultList();
        return emailInfos;
    }

    @Override
    public EmailInfo getEmail(long id) {
        return null;
    }

    @Override
    public void updateEmail(EmailInfo fbkFile) {

    }

    @Override
    public void delEmail(int id) {
        EmailInfo emailInfo = entityManager.find(EmailInfo.class, id);
        entityManager.remove(emailInfo);
        entityManager.flush();
        entityManager.clear();
    }

    @Override
    public boolean isFileExist(EmailInfo emailInfo) {
        String hql = "FROM EmailInfo WHERE title=:title AND toEmail=:toEmail";
        int count = entityManager.createQuery(hql).setParameter("toEmail", emailInfo.getToEmail()).setParameter("title", emailInfo.getTitle())
                .getResultList().size();
        return count > 0 ? true : false;
    }

    @Override
    public int createEmail(EmailInfo emailInfo) {
        entityManager.persist(emailInfo);
        entityManager.flush();
        return emailInfo.getPushId();
    }

    @Override
    public void saveHis(int pushId) {
        EmailInfo emailInfo = entityManager.find(EmailInfo.class, pushId);
        EmailHisInfo emailHisInfo = new EmailHisInfo();
        emailHisInfo.setPushId(emailInfo.getPushId());
        emailHisInfo.setEmailTp(emailInfo.getEmailTp());
        emailHisInfo.setFromEmail(emailInfo.getFromEmail());
        emailHisInfo.setToEmail(emailInfo.getToEmail());
        emailHisInfo.setTitle(emailInfo.getTitle());
        emailHisInfo.setContent(emailInfo.getContent());
        emailHisInfo.setSenderNm(emailInfo.getSenderNm());
        emailHisInfo.setReqDt(emailInfo.getReqDt());
        emailHisInfo.setSentDt(new Date().getTime());
        emailHisInfo.setStatus(1);
        entityManager.persist(emailHisInfo);
        entityManager.flush();
    }
}

