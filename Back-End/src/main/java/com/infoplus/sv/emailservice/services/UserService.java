package com.infoplus.sv.emailservice.services;


import org.springframework.security.core.userdetails.UserDetails;

public interface UserService {

    UserDetails getUser(String userName);

    int createUser(String userName, String password);

    boolean isUser(String userName);

    boolean isPwd(String userName, String password);
}
