package com.infoplus.sv.emailservice.model;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import java.util.Date;
import java.util.List;

@Data
public class EmailModel {
    private int pushId;
    @Email
    @NotEmpty
    private String fromEmail;
    private String toEmail;
    @NotEmpty
    private List<String> toEmailList;
    @NotEmpty
    private String title;
    @NotEmpty
    private String content;
    private String senderNm;
    private Date requestDt;
    private int emailTp;
}
