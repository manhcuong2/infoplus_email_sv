package com.infoplus.sv.emailservice.model;

import lombok.Data;

@Data
public class UserModel {
    private String userName;
    private String password;
    private String rePassword;
}
