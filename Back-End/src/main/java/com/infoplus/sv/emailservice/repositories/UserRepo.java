package com.infoplus.sv.emailservice.repositories;


import com.infoplus.sv.emailservice.domain.UserInfo;

import java.util.List;

public interface UserRepo {
    UserInfo getUserInfo(String userId);

    List<UserInfo> listUserInfo();

    int addUser(UserInfo userInfo);

    void updateUser(UserInfo userInfo);

    void deleteUser(String id);

    boolean isUser(String userName);
}
