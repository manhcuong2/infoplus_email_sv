package com.infoplus.sv.emailservice.services.impl;

import com.infoplus.sv.emailservice.config.Config;
import com.infoplus.sv.emailservice.domain.UserInfo;
import com.infoplus.sv.emailservice.model.UserModel;
import com.infoplus.sv.emailservice.repositories.UserRepo;
import com.infoplus.sv.emailservice.security.WebSecurityConfig;
import com.infoplus.sv.emailservice.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepo userRepo;

    @Autowired
    private Config fbkConfig;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public UserDetails getUser(String userName) {
        UserInfo userInfo = userRepo.getUserInfo(userName);
        return new User(userInfo.getUserName(), userInfo.getPassword(), new ArrayList<>());
    }

    @Override
    public int createUser(String userName, String password) {
        UserInfo userInfo = new UserInfo();
        userInfo.setUserName(userName);
        userInfo.setPassword(passwordEncoder.encode(password));
        userInfo.setCreateDate(new Date());
        int userId = userRepo.addUser(userInfo);
        return userId;
    }

    @Override
    public boolean isUser(String userName) {
        return userRepo.isUser(userName);
    }

    @Override
    public boolean isPwd(String userName, String password) {
        UserInfo userInfo = userRepo.getUserInfo(userName);
        boolean isPwd = passwordEncoder.matches(password, userInfo.getPassword());
        return isPwd;
    }

}
