package com.infoplus.sv.emailservice.repositories.impl;

import com.infoplus.sv.emailservice.domain.UserInfo;
import com.infoplus.sv.emailservice.repositories.UserRepo;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;


@Repository
@Transactional
public class UserRepoImpl implements UserRepo {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public UserInfo getUserInfo(String userName) {
        String hql = "FROM UserInfo WHERE userName=:userName";
        UserInfo user = (UserInfo) entityManager.createQuery(hql).setParameter("userName", userName).getSingleResult();
        return user;
    }

    @Override
    public List<UserInfo> listUserInfo() {
        return null;
    }

    @Override
    public int addUser(UserInfo userInfo) {
        entityManager.persist(userInfo);
        entityManager.flush();
        return userInfo.getUserId();
    }

    @Override
    public void updateUser(UserInfo userInfo) {

    }

    @Override
    public void deleteUser(String id) {

    }

    @Override
    public boolean isUser(String userName) {
        String hql = "FROM UserInfo WHERE userName=:userName";
        int count = entityManager.createQuery(hql).setParameter("userName", userName)
                .getResultList().size();
        return count > 0 ? true : false;
    }
}

